<?php

/**
 * @file
 * Node Auto Expire.
 *
 * Install, update and uninstall functions for the Node Auto Expire module.
 */

use Drupal\node\Entity\NodeType;

/**
 * Implements hook_schema().
 */
function node_auto_expire_schema() {

  $schema = [];

  $schema['node_auto_expire'] = [
    'description' => 'Auto expire module: track node expirations',
    'fields' => [
      'nid' => [
        'description' => 'node id: primary key',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'warned' => [
        'description' => 'warned flag: whether email has been sent out',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'expire' => [
        'description' => 'expire date: Unix timestamp',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'extended' => [
        'description' => 'number of times the node expiry has been extended',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['nid'],
  ];

  return $schema;
}

/**
 * Delete states.
 *
 * Implements hook_uninstall().
 */
function node_auto_expire_uninstall() {

  $drupal_state = Drupal::state();
  $config = Drupal::config('node_auto_expire.settings');

  $email_texts = [
    'warn_subject',
    'warn_body',
    'expired_subject',
    'expired_body',
  ];
  $type_codes = ['_e', '_d', '_w', '_p', '_ex'];

  foreach (NodeType::loadMultiple() as $type => $name) {

    $code = $config->get('node_auto_expire_node_type') . $type;

    foreach ($type_codes as $type_code) {
      $drupal_state->delete($code . $type_code);
    }

  }

  foreach ($email_texts as $email_text) {
    $drupal_state->delete($config->get('node_auto_expire_email') . $email_text);
  }

}
